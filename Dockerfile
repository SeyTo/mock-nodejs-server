FROM node:10
# create app directory
WORKDIR /usr/src/app
# install app dependencies
COPY package*.json ./

RUN npm install

COPY . .
EXPOSE 8000
CMD [ "node", "index.js" ]
