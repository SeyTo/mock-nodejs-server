const express = require('express')

const app = express()
const port = 8001;

var switchh = false;

app.post('/refresh', (req, res) => {
  console.log("/refresh at " + Date.now())
  switchh = true
  res.send({ success: switchh, message: "Switch is " + switchh })
})

app.get('/check', (req, res) => {
  console.log("/check at " + Date.now())
  res.send({ success: true, message: "Switch is " + switchh })
})

app.post('/toggle', (req, res) => {
  switchh = !switchh
  console.log("/toggle at " + Date.now())
  res.send({ success: switchh, message: "Switch is " + switchh })
})


app.post('/Token/RequestTokenForCustomer', (req, res) => {
  // if (!switchh) {
    console.log('/login 200 failed at ' + Date.now())
    res.status(200).send()
  // } else {
    // console.log('/login success at ' + Date.now())
    // res.send({ success: switchh, message: "Switch is " + switchh })
  // }
})

app.post('/Token/Refresh/refresh', (req, res) => {
  res.status(200).send({
    "status": null,
    "success": true,
    "message": "Success",
    "data": {
      "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhc2h1LmdtNTRAZ21haWwuY29tIiwianRpIjoiMzMzY2FkMjUtMTJkZC00NzUwLTliZDAtOTk0N2IzNzFkZjU5IiwiaWF0IjoxNTczMjE2NzgzLCJJZCI6IjE3dkpUaGh4M2d5dVVtaWVyK1p6S0x3Q1pUQnhNYXpNIiwiUm9sZSI6IkN1c3RvbWVyIiwibmJmIjoxNTczMjE2NzgyLCJleHAiOjE1NzMyMjM5ODIsImlzcyI6IndlYkFwaSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6NTAwMC8ifQ.Q4I9Bmn-MkqlshoXtm_rEx-CAlN5r3JltSalys3j5lI",
      "refreshToken": "asdflak",
      "expireTime": "asdf"
    }
  })
})

app.listen(port, () => console.log("at " + port))
